/**
 * Created by PhpStorm.
 * User: ANDREW-LH532
 * Date: 29.07.2016
 * Time: 8:22
 */
<!DOCTYPE html>
<head>
    <meta charset="UTF-8" />
    <title>Посчитаем деньги!</title>
    <!-- <link href="css/main.css" rel="stylesheet"> -->
</head>
<body>
    <form method="POST" action="oklad.php">
        <h4>
        Введите Ваш оклад <br/>
        <input type="number" name="oklad" step="100" value="<?=$_POST['oklad'] ?? '22700'?>" required/>
        <br/> Ваше КТУ <br/>
        <input type="number" name="ktu" step="0.05" value="<?=$_POST['ktu'] ?? '1.00'?>" required/>
        <br/> Размер премии, целое число <br/>
        <input type="number" name="premia" step="0.01" value="<?=$_POST['premia'] ?? '30.0'?>" />
        <br/> или полученная сумма(тогда получите прикидку сколько процентов) <br/>
        <input type="number" name="summa" value="<?=$_POST['summa'] ?? '0'?>" />
        <br/> Часов переработки (в СОТЫХ часа) <br/>
        <input type="number" name="chas" step="0.01" value="<?=$_POST['chas'] ?? '0'?>" />
        <br/> Налог <br/>
        <input type="number" name="nalog" min="10" max="50" value="<?=$_POST['nalog'] ?? '13'?>" required/>
        <input type="submit" name="ok_go" value="GO!" />
        </h4>
    </form>
    <?php
    if(isset($_POST['ok_go'])){ #если нажата клавиша формы
        $premia = $_POST['premia'];#присваиваем значение первого поля переменной
        //$first_var = trim($first_var); #убираем пробелы по краям, если они есть
        $oklad = $_POST['oklad'];
        $ktu = $_POST['ktu'];
        $summa = $_POST['summa'];
        $nalog = $_POST['nalog'];
        $chas = $_POST['chas'];
        if(empty($premia) && empty($summa)) echo 'Пусто! вы что то не ввели'; #если пустая, выводим Пусто!
        elseif (empty($summa)){ #не пустая
            $itog = (($oklad / 100) * $premia) * $ktu;
            $itog2 = $itog * (1 - $nalog / 100);
            $avans = $oklad /10 * 4;
            echo 'Сумма премии: <b>', $itog, '</b><br>';
            echo 'С вычетом налога: <b>', $itog2 , '</b><br>';
            echo 'Размер аванса: <b>', $avans, '</b><br/>';
            echo '<h3> Суммарно: <b>', $avans + $itog2, '</b></h3><br>';
            $nalog_avans = $avans / 100 * $nalog;
            $ostatok = ($oklad / 10 * 6) * (1 - $nalog / 100) - $nalog_avans;
            echo 'Что еще осталось (без переработок): <b>', $ostatok, '</b><br>';
            if (!empty($chas)) {
                $godovaya_norma_chasov = 1973 ;
                $chas_pererab = $oklad / ($godovaya_norma_chasov / 12) * 2;
                $summa_pererab = $chas * $chas_pererab;
                $clear_summ_pererab = $summa_pererab * (1 - $nalog / 100);
                echo 'Сумма за ', $chas, ' часов переработки: <b>', $summa_pererab, '</b><br>';
                echo 'С вычетом налога: <b>', $clear_summ_pererab , '</b><br>';
                echo 'С вычетом налога и остатком: <b>', $clear_summ_pererab + $ostatok, '</b><br>';
                echo '<h1> Суммарно: <b>', $avans + $itog2 + $ostatok + $clear_summ_pererab, '</b></h1><br>';
            }
        }
        else{
            $itog = ($summa / $ktu) / (($oklad * (1 - $nalog / 100)) / 100);
            echo 'Процент премии: <b>', $itog, '</b>';

        }
    }
    $_POST['ok_go'] = 0;
    ?>
</body>
