<?php
  $h = date('H');
  $img = (int) ($h / 6);
  $m = date('i');
  $minuteWord = function($a) {
    switch (true) {
      case ($a > 10 && $a < 15):
        $word = 'минут';
        break;
      case (substr($a, 1) == '1'):
        $word = 'минута';
        break;
      case (substr($a, 1) > 1 && substr($a, 1) < 5):  
        $word = 'минуты';
        break;
      default:
        $word = 'минут';
        break;
    };
    return $word;
  };
 ?>
<!Doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title>Добро пожаловать</title>
  <link href="css/main.css" rel="stylesheet">
  <link href="css/animate.css" rel="stylesheet">
  <style>
    body {
      background: url(img/<?php echo $img; ?>.jpg);
      background-size: cover;
    }
  </style>
</head>
<body>
<div id="wrapper" class="zoomInUp animated">
  <h1>Вас приветствует тестовый сервер.</h1><?php echo $m . ' ' . $minuteWord($m);?>
  <p>Потихоньку он будет развиваться и дополняться, пока можете посмотреть на тесты ниже.
  В ближайших планах добавить авторизацию и обратную связь, которая поможет построить
  систему нашего управления совместными усилиями.</p>
  <h2>Для нормальной работы рекомендуется использовать:</h2>
  <a href="distr/ChromeStandaloneSetup53.exe">Google Chrome</a> или 
  <a href="distr/FirefoxSetup49.0.1.exe">Mozilla Firefox</a>
  а так же любые другие современные браузеры, но работоспособность не гарантируется.<br>
  <strong>Internet Explorer не поддерживается и не будет такого никогда.</strong>
  <ol>
    <li><a href="animtest.html">браузерная анимация</a></li>
    <li><a href="screenwide.html">тест ширины экрана</a></li>
    <li><a href="speech.html">речь (нужен микрофон)</a></li>
    <li><a href="textanim.html">анимация текста</a></li>
    <li><a href="oklad.php">оклад</a></li>
    <li><a href="ip.html">ай пи</a></li>
    <li><a href="regform.html">форма</a></li>
    <li><a href="cube.html">три дэ куб</a></li>
    <li><a href="telebot.html">написать пикулику на мобильный (без обратной связи)</a></li>
    <li><a href="StoryTelling/demo/index.html">путешествие Индианы Джонса по бескрайним просторам Африки.</a></li>
    <li><a href="ya.html">Яндекс карты</a></li>
  </ol>
 </div>
</body>  