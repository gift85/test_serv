window.onload = function () {
	var inp_message = document.querySelector('input[name=message]');
	var inp_name = document.querySelector('input[name=name]');
	document.querySelector('#send').onclick = function () {
		var params = 'name=' + inp_name.value + '&' + 'message=' + inp_message.value;
		ajaxPost(params);
	}
}
function ajaxPost(params) {
	var request = new XMLHttpRequest();
	
	request.onreadystatechange = function () {
		if (request.readyState == 4 && request.status == 200) {
			if (request.responseText == '1'){
				document.querySelector('#result').innerHTML = 'Ура -ура! Все хорошо!';
				//document.querySelector('form').style.display = 'none';
			} else {
				document.querySelector('#result').innerHTML = request.responseText;
			}
		}
	}
	
	request.open('POST', 'telebot.php');
	request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	request.send(params);
}