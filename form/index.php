<?php
    echo '<pre>';
    print_r($_SERVER["HTTP_USER_AGENT"]);
    echo '</pre>';
    
    if(count($_POST) > 0){
        $name = $_POST['name'];
        $phone = $_POST['phone'];
        
        if(strlen($name) < 3) {
          echo 'Введите более длинное имя!<br>';
        } else
        {
          echo 'С именем все хорошо<br>';
          
          if(!preg_match("/^\d+$/", $phone)) {
            echo 'В номере телефона только цифры!<br>';
          } else
          {
            echo 'Все нормально с номером<br>';
            $d = date('Y-m-d H:i:s');
            file_put_contents('apps.txt', "$d $name $phone\n", FILE_APPEND);
            echo 'Ваша заявка принята, ожидайте звонка!<br>';
          };
        };
    };
?>

<form method="post">
    Имя<br>
    <input type="text" name="name" <?php 
    if (isset($_POST['name'])) {
      echo 'value=' . $name;
    };
    ?> required><br>
    Телефон<br>
    <input type="text" name="phone" <?php 
    if (isset($_POST['phone'])) {
      echo 'value=' . $phone;
    };
    ?>><br>
    <input type="submit" value="Отправить">
</form>

<?php
      
    $capitals = [
        'Россия' => ['Москва', 'Санкт-Петербург', 'Новосибирск'], 
        'Франция' => ['Париж', 'Марсель', 'Лион', 'Ницца'],  
        'Англия' => ['Лондон', 'Ливерпуль', 'Бирмингем']
    ];
    echo '<ul>';
    foreach($capitals as $country => $cities) {
      echo "<li>$country<ol>";
      foreach($cities as $city) {
        echo "<li>$city</li>";
      };
      echo '</ol></li>';
    };
    echo '</ul>';
    /* ul-li-ul-li */
    ?>